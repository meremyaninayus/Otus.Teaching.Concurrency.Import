﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    class ThreadDataLoader : IDataLoader
    {
        private List<Customer> _customers;
        private int _maxNumberOfRetries;
        private int _numberOfThreads;
        ICustomerRepository _repository;
        public ThreadDataLoader(List<Customer> customers, int maxNumberOfRetries, ICustomerRepository repository, int numberOfThreads)
        {
            _customers = customers;
            _maxNumberOfRetries = maxNumberOfRetries;
            _repository = repository;
            _numberOfThreads = numberOfThreads;
        }
        public void LoadData()
        {
            Console.WriteLine("Loading data...");

            var sw = new Stopwatch();
            sw.Start();

            List<Thread> threadList = new List<Thread>();
            for (int i = 0; i < _numberOfThreads; i++)
            {
                var customerChunkParameters = new CustomerChunkParameters();
                customerChunkParameters.ChunkSize = _customers.Count / _numberOfThreads;
                customerChunkParameters.StartIndex = customerChunkParameters.ChunkSize * i;
                var thread = new Thread(new ParameterizedThreadStart(LoadChunk));
                thread.Start(customerChunkParameters);
                threadList.Add(thread);
            }
            foreach (Thread thread in threadList)
            { 
                thread.Join();
            }
            sw.Stop();
            Console.WriteLine($"Loaded data in {sw.ElapsedMilliseconds} ms");
        }

        private void LoadChunk(object obj)
        {
            var customerChunkParameters = (CustomerChunkParameters)obj;
            for (int i = customerChunkParameters.StartIndex; i < customerChunkParameters.StartIndex + customerChunkParameters.ChunkSize; i++)
            {
                if (!Retrier.Retry(() => _repository.AddCustomerAsync(_customers[i]), _maxNumberOfRetries))
                    break;    
            }
        }


        class CustomerChunkParameters
        {
            public int StartIndex {get;set; }
            public int ChunkSize { get; set; }
        }

    }
}

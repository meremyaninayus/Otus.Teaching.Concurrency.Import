﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPICustomers.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        // POST api/Customers
        /// <summary>
        /// Создаем человека
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<Customer>> Post([FromBody] Customer customer)
        {
            var customerRepository = new CustomerRepository();
            var customerInBase = await customerRepository.GetCustomerAsync(customer.Id.ToString());
            if (customerInBase == null)
            {
                await customerRepository.AddCustomerAsync(customer);
                return Ok();
            }
            else
                return Conflict();
        }

        // GET api/Customers/5
        // https://docs.microsoft.com/ru-ru/aspnet/core/web-api/action-return-types?view=aspnetcore-5.0#asynchronous-action
        /// <summary>
        /// Получаем человека по id
        /// </summary>
        /// <param name="id">id человека</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(string id)
        {
            var customerRepository = new CustomerRepository();
            var customer = await customerRepository.GetCustomerAsync(id);
            if (customer == null)
            {
                return NotFound();
            }
            return Ok(customer);
        }
    }
}
